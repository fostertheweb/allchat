var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

// Routing
app.use(express.static(__dirname + '/public'));

var usernames = {};
var userCount = 0;

io.on('connection', function (socket) {
  var addedUser = false;

  socket.on('chat message', function (data) {
    socket.broadcast.emit('chat message', {
      username: socket.username,
      message: data
    });
  });

  socket.on('add user', function (username) {
    socket.username = username;

    usernames[username] = username;
    ++userCount;

    addedUser = true;
    socket.emit('user joined', {
      userCount: userCount
    });

    socket.broadcast.emit('user joined', {
      username: socket.username,
      userCount: userCount
    });
  });

  socket.on('disconnect', function () {
    if (addedUser) {
      delete usernames[socket.username];
      --userCount;

      socket.broadcast.emit('user disconnected', {
        username: socket.username,
        userCount: userCount
      });
    }
  });

  socket.on('chat message', function (message) {
    io.emit('chat message', message);
  });
});

http.listen(port, function () {
  console.log('listening on port ' + port);
});


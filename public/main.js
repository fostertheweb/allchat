var socket = io(); 

$('form').submit(function () {
  socket.emit('chat message', $('#message').val());
  $('#message').val('');
  return false;
});

socket.on('chat message', function (message) {
  $('#messages').append($('<li>').text(message));
});

socket.on('user connected', function (data) {
  $('#messages').append($('<li>').text(data.message));
});

socket.on('user disconnected', function (data) {
  $('#messages').append($('<li>').text(data.message));
});

